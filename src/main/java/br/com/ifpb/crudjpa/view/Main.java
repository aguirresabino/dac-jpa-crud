package br.com.ifpb.crudjpa.view;

import br.com.ifpb.crudjpa.dao.PessoaDao;
import br.com.ifpb.crudjpa.entidade.Pessoa;

public class Main {

    public static void main(String[] args) {

        PessoaDao dao = new PessoaDao();
        Pessoa p = new Pessoa();
        p.setEmail("flavio@gmail.com");
        p.setNome("Flavio Henrique");
        p.setTelefone("101010101010");

        dao.create(p);
        dao.create(new Pessoa("aguirre@gmail.com", "Aguirre", "10928393"));

        System.out.println("Pessoa salvas");

        System.out.println("Listando todas:");
        dao.read().forEach(System.out::println);

        p.setNome("Teste update");
        System.out.println("Atualizando pessoa");

        dao.update(p);
        System.out.println("Listando todas:");
        dao.read().forEach(System.out::println);

        System.out.println("Deletando:");
        dao.delete(p);

        System.out.println("Listando todas:");
        dao.read().forEach(System.out::println);

    }
}
