/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.crudjpa.dao;

import br.com.ifpb.crudjpa.entidade.Pessoa;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author aguirresabino
 */
public class PessoaDao {
    private final EntityManager entityManager;
    
    public PessoaDao(){
        this.entityManager = Persistence.createEntityManagerFactory("PUpostgres")
                                        .createEntityManager();
    }
    
    public void create(Pessoa pessoa){
        entityManager.getTransaction().begin();

        entityManager.persist(pessoa);
        entityManager.getTransaction().commit();
    }
    
    public List<Pessoa> read(){
        entityManager.getTransaction().begin();
        
        List<Pessoa> pessoas = entityManager.createQuery("SELECT p FROM Pessoa p").getResultList();
        
        entityManager.getTransaction().commit();
        
        return pessoas;
    }
    
    public void update(Pessoa pessoa) {
        entityManager.getTransaction().begin();
        
        entityManager.merge(pessoa);
        entityManager.getTransaction().commit();
    }
    
    public void delete(Pessoa pessoa){
        entityManager.getTransaction().begin();
        entityManager.remove(pessoa);
        entityManager.getTransaction().commit();
    }
}
